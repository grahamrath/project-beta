# CarCar

Team:

* Troy Accos - Service
* Graham Rath - Sales

## How to Run this App
1. Fork the following repository: https://gitlab.com/grahamrath/project-beta

2. Clone the forked repository onto your local computer using: git clone https://gitlab.com/grahamrath/project-beta.git

3. Build and run the project using Docker with the following commands:
    * docker volume create beta-data
    * docker-compose build
    * docker-compose up

4. Once you've run those commands, check and make sure your Docker containers are up and running properly

5. You should now be able to navigate to: http://localhost:3000/  in your browser and see your project

## Diagram of CarCar
```
See CarCarDiagram.png file in Project-Beta directory
```

## Design

Our app CarCar is comprised of 3 microservices that all interact with one another.
 - Service
 - Sales
 - Inventory

## Inventory

Explain your models and integration with the inventory
microservice, here.

(We decided to make this optional)




## Service microservice

Hi, this is Troy and i'll be your guide throught the service microservice. Here, we keep track of appointments, technicians, and customers.

When an automobile that has been purchased from our inventory comes in for a service appointment, we identify them as a vip.

### Service Microservice Value Objects
The Service microservice has 4 models:
	* AutomobileVO - this Value Object pulls in data about the automobiles from inventory via the poller.  The poller pulls the data every minute automatically.
	* Techician - the employee that will be servicing the automobile
	* Appointment - which has the necessary fields to schedule an appointment. It has a foreignkey to technician, which will allow you to choose the technician to handle the service appointment.

## Accessing Endpoints to Send and View Data - Access through Insomnia:

### Technicians:

These are the endpoints to send and view data for technicians through Insomnia:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List tehnicians | GET | http://localhost:8080/api/technicians/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Show technician details | GET | http://localhost:8080/api/technicians/id/
| Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:pk>/

To create a technician, we used this JSON body in the POST method. There is no id required in the url because it is a GET method:
...
{
	"first_name": "Will",
	"last_name": "Ravengard",
	"employee_id": "WRavengard"
}
...
The return value of creating a technician with a 200 OK response. The id field is automatically generated when a technician is created:
...
{
	"first_name": "Will",
	"last_name": "Ravengard",
	"employee_id": "WRavengard"
	"id": 1
}
...
The return value of listing all technicians. Following the list technicians url will display a list of all the technicians made so far. There is no need to put an id value in the url or anything in the json body because it is a GET method:
...
{
	"technician": [
		{
			"first_name": "Enver",
			"last_name": "Gortash",
			"employee_id": "EGortash",
			"id": 1
		},
		{
			"first_name": "Ketheric",
			"last_name": "Thorm",
			"employee_id": "KThorm",
			"id": 2
		},
		{
			"first_name": "Duke",
			"last_name": "Ravengard",
			"employee_id": "DRavengard",
			"id": 3
		}
	]
}
...
To show a specific technician, we use the GET url again, but with the technician's "id" value at the end. This will grab the single technician with the matching id value, and display it in the response. There is no need to type anything in the json body since it is a GET request. This is what the response will look like if we use "" in the url:
...
{
	"technician": {
		"first_name": "Will",
		"last_name": "Ravengard",
		"employee_id": "WRavengard",
		"id": 1
	}
}
...
Deleting a technician - If we want to delete a technician (or make a person disappear), you need to change the request type in insomnia to DELETE and use the url for the delete method. Though it says int:pk at the end, you replace that with the specific technician's id like in the detail url. So if we were to delete the technician "Will", we would use his id value in the url just like how we accessed his details in the GET method. There is no need for anything in the json body, since we are deleting. This is what the response should look like if the request went through:
...
{
	"deleted": true
}
...

With these api endpoints and json bodies, you will be able to view a list of all technicians, a specific technician, and delete a specific technician from the database.
...


### Service appointments

These are the api endpoints for the service appointments in Insomnia

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List service appointments | GET | http://localhost:8080/api/appointments/
| Service appointment detail | GET | http://localhost:8080/api/appointments/<int:pk>/
| Create service appointment | POST | http://localhost:8080/api/appointments/
| Delete service appointment | DELETE | http://localhost:8080/api/appointments/<int:pk>/
| Update finish appointment | PUT | http://localhost:8080/api/appointments/:id/finish/
| Update canceld appointment | PUT | http://localhost:8080/api/appointments/:id/cancel/

Creating a service appointment - This is the POST method that will create a new service appontment. The Insomnia request must be set to POST and you must use the url above to create a new service appointment. In the JSON body, you MUST use this format to create a new appointment, especially when it comes to the date_time field. We access the technician throught :
...
{
	"vin": "1C3CC5FB2AN121CDF",
	"customer": "Me Mi",
	"date_time": "2023-04-20T14:39:00+00:00",
	"technician": 1,
	"reason": "It's not about the car, it's just me"
}
...
You should get a response like this when the response is 200 OK. The id is generated, and the appointment status is automatically set to "pending"
...
{
	"vin": "1C3CC5FB2AN121CDF",
	"customer": "Ti To",
	"date_time": "2023-04-20T14:39:00+00:00",
	"technician": {
		"first_name": "Enver",
		"last_name": "Gortash",
		"employee_id": "EGortash",
		"id": 1
	},
	"reason": "It's not about the car, it's just me",
	"status": "pending",
	"id": 12
}
...

Listing the service appointments - This action uses the GET method and the http://localhost:8080/api/appointments/ url. The url doesn't need an id and you don't need to type anything in the json body. When you send the request, you should get a list of all the appointments that have been created so far:
...
{
	"appointments": [
		{
			"vin": "1C3CC5FB2AN121CCA",
			"customer": "Will TheBladeOfFrontiers",
			"date_time": "2023-04-20T14:39:00+00:00",
			"technician": {
				"first_name": "Duke",
				"last_name": "Ravengard",
				"employee_id": "DRavengard",
				"id": 3
			},
			"reason": "I ran off with a devilish lady and she ruined my life",
			"status": "finished",
			"id": 4
		},
		{
			"vin": "1C3CC5FB2AN121CCD",
			"customer": "Karlach Himbo",
			"date_time": "2023-04-20T14:39:00+00:00",
			"technician": {
				"first_name": "Enver",
				"last_name": "Gortash",
				"employee_id": "EGortash",
				"id": 1
			},
			"reason": "My engine's running hot",
			"status": "canceled",
			"id": 2
		},
		{
			"vin": "1C3CC5FB2AN121CCV",
			"customer": "Tav Champion",
			"date_time": "2023-04-20T14:39:00+00:00",
			"technician": {
				"first_name": "Ketheric",
				"last_name": "Thorm",
				"employee_id": "KThorm",
				"id": 2
			},
			"reason": "Need oil change too",
			"status": "finished",
			"id": 1
		}
	]
}
...

Getting the details of a specific appointment - You must have the request method to GET and use the url listed above for the appointment details. Though it says "int:pk", you replace that with the id value of that appointment. It should look like http://localhost:8080/api/appointments/1/ to grab the appointment with an id value of 1. If the response goes through, you should get the 200 OK response code and the details of that appointment:
...
{
	"appointment": {
		"vin": "1C3CC5FB2AN121CDA",
		"customer": "Me Mo",
		"date_time": "2023-04-20T14:39:00+00:00",
		"technician": {
			"first_name": "Enver",
			"last_name": "Gortash",
			"employee_id": "EGortash",
			"id": 1
		},
		"reason": "It's not about the car, it's just me",
		"status": "pending",
		"id": 1
	}
}
...

Deleting a specific appointment - To delete a specific appointment, the request method in insomnia should be set to delete and you should use the api endpoint for the delete method which is http://localhost:8080/api/appointments/<int:pk>/. You must replace the <int:pk> with the specific id number for the appointment you want to delete like above. Nothing is required in the JSON body. When you send the response, it should look like this if it goes through:
...
{
	"deleted": true
}
...

Updating appointment status to "finished" - This request will uodate the "pending" status of an appointment to "finished." The request method must be set to PUT and the url should be http://localhost:8080/api/appointments/:id/finish/. The :id should be replaced with the id value of the specific appointment you want to update, and the JSON body should contain:
...
{
	"finished": "True"
}
...
the returned response if it comes back as a 200 OK should update the "status" field of the appointment to "finished" like so:
...
{
	"vin": "1C3CC5FB2AN121CCA",
	"customer": "Will TheBladeOfFrontiers",
	"date_time": "2023-04-20T14:39:00+00:00",
	"technician": {
		"first_name": "Duke",
		"last_name": "Ravengard",
		"employee_id": "DRavengard",
		"id": 3
	},
	"reason": "I ran off with a devilish lady and she ruined my life",
	"status": "finished",
	"id": 4
}
...

Updating appointment status to "canceled" - This request will uodate the "pending" status of an appointment to "canceled." The request method must be set to PUT and the url should be http://localhost:8080/api/appointments/:id/canceled/. The :id should be replaced with the id value of the specific appointment you want to update, and the JSON body should contain:
...
{
	"canceled": "True"
}
...

the returned response if it comes back as a 200 OK should update the "status" field of the appointment to "canceled" like so:
...
{
	"vin": "1C3CC5FB2AN121CCD",
	"customer": "Karlach Himbo",
	"date_time": "2023-04-20T14:39:00+00:00",
	"technician": {
		"first_name": "Enver",
		"last_name": "Gortash",
		"employee_id": "EGortash",
		"id": 1
	},
	"reason": "My engine's running hot",
	"status": "canceled",
	"id": 2
}
...



######



## Sales microservice
The Sales microservice is made up of 4 models:
    * AutomobileVO - this Value Object pulls in data about the automobiles from inventory via the poller.  The poller pulls the data every minute automatically.
    * Salesperson
    * Customer
    * Sale - this models has 3 foreign keys on it and receives data from the other 3 models

The Sales microservice and the Inventory microservice are linked because you need the automobile data to sell the car and then you need the inventory to update so you can re-sell a VIN / automobile that was already sold.

Use Insomnia to access all API endpoints, to send, and to view data.

### For Salespeople:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople| GET | http://localhost:8090/api/salespeople/
| Create a Salesperson | POST | http://localhost:8090/api/salespeople/
| Show a specific salesperson| GET | http://localhost:8090/api/salespeople/id/

To create a Salesperson, send this JSON body:
```
{
  "first_name": "Rando",
  "last_name": "Guy",
  "employee_id": "rguy"
}
```
The  Return the value of a created Salesperson:
```
{
    "first_name": "Rando",
    "last_name": "Guy",
    "employee_id": "rguy",
    "id": 3
}
```
The Return Value of listing all Salespeople:
```
{
	"salesperson": [
		{
			"first_name": "Graham",
			"last_name": "Rath",
			"employee_id": "grath",
			"id": 1
		},
		{
			"first_name": "Troy",
			"last_name": "Accos",
			"employee_id": "taccos",
			"id": 2
		},
		{
			"first_name": "Rando",
			"last_name": "Guy",
			"employee_id": "rguy",
			"id": 3
		}
	]
}
```
### For Customers
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers| GET | http://localhost:8090/api/customers/
| Create a Customer | POST | http://localhost:8090/api/customers/
| Show a specific customer| GET | http://localhost:8090/api/customers/id/

To create a customer, send this JSON body:
```
{
    "first_name": "Ron ",
    "last_name": "Swanson",
    "address": "555 Nunya St. Pawnee, IN 87452",
    "phone_number": 8758655214,
}
```
The  Return the value of a created customer:
```
{
    "first_name": "Ron ",
    "last_name": "Swanson",
    "address": "555 Nunya St. Pawnee, IN 87452",
    "phone_number": 8758655214,
    "id": 1
}
```
The Return Value of listing all customers:
```
{
	"customer": [
		{
			"first_name": "Ron ",
			"last_name": "Swanson",
			"address": "555 Nunya St. Pawnee, IN 87452",
			"phone_number": 8758655214,
			"id": 1
		},
		{
			"first_name": "Leslie",
			"last_name": "Knope",
			"address": "1658 Government Way Pawnee, IN78524",
			"phone_number": 7852451254,
			"id": 2
		}
	]
}
```
### For Sales
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List sales| GET | http://localhost:8090/api/sales/
| Create a new sale | POST | http://localhost:8090/api/sales/
| Show a specific sale| GET | http://localhost:8090/api/sales/5/

To create a new sale, send this JSON body:
```
{
    "automobile": "2J3ZE4BL2AM127735",
    "salesperson": 3,
    "customer": 2,
    "price": "25000"
}
```
The  Return the value of a created sale:
```
{
	"automobile": {
		"vin": "2J3ZE4BL2AM127735",
		"sold": true
	},
	"salesperson": {
		"first_name": "Rando",
		"last_name": "Guy",
		"employee_id": "rguy",
		"id": 3
	},
	"customer": {
        "first_name": "Ron ",
        "last_name": "Swanson",
        "address": "555 Nunya St. Pawnee, IN 87452",
        "phone_number": 8758655214,
        "id": 1
	},
	"price": 25000,
	"id": 1
}
```
Show a specific sale details:
```
{
	"automobile": {
		"vin": "1E3CD5VL2AG124294",
		"sold": true
	},
	"salesperson": {
		"first_name": "Rando",
		"last_name": "Guy",
		"employee_id": "rguy",
		"id": 3
	},
	"customer": {
		"first_name": "Leslie",
		"last_name": "Knope",
		"address": "1658 Government Way Pawnee, IN78524",
		"phone_number": 7852451254,
		"id": 2
	},
	"price": 12000,
	"id": 3
}
```
The Return Value of listing all sales:
```
{
	"sale": [
		{
			"automobile": {
				"vin": "1C3CC5FB2AN120174",
				"sold": true
			},
			"salesperson": {
				"first_name": "Graham",
				"last_name": "Rath",
				"employee_id": "grath",
				"id": 1
			},
			"customer": {
				"first_name": "Ron ",
				"last_name": "Swanson",
				"address": "555 Nunya St. Pawnee, IN 87452",
				"phone_number": 8758655214,
				"id": 1
			},
			"price": 35000,
			"id": 1
		},
		{
			"automobile": {
				"vin": "2F3CE5BL2AM127145",
				"sold": true
			},
			"salesperson": {
				"first_name": "Troy",
				"last_name": "Accos",
				"employee_id": "taccos",
				"id": 2
			},
			"customer": {
				"first_name": "Leslie",
				"last_name": "Knope",
				"address": "1658 Government Way Pawnee, IN78524",
				"phone_number": 7852451254,
				"id": 2
			},
			"price": 45000,
			"id": 2
		},
		{
			"automobile": {
				"vin": "1E3CD5VL2AG124294",
				"sold": true
			},
			"salesperson": {
				"first_name": "Rando",
				"last_name": "Guy",
				"employee_id": "rguy",
				"id": 3
			},
			"customer": {
				"first_name": "Leslie",
				"last_name": "Knope",
				"address": "1658 Government Way Pawnee, IN78524",
				"phone_number": 7852451254,
				"id": 2
			},
			"price": 12000,
			"id": 3
		}
	]
}
```
