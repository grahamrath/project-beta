import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AutomobilesForm from './AutomobilesForm';
import AutomobilesList from './AutomobilesList';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import ModelsForm from './ModelsForm';
import ModelsList from './ModelsList';
import SaleForm from './SaleForm';
import SaleList from './SaleList';
import SalespersonForm from './SalespersonForm';
import SalespeopleList from './SalespeopleList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import AppointmentsForm from './AppointmentsForm';
import AppointmentsList from './AppointmentsList';
import TechnicianForm from './TechniciansForm';
import TechnicianList from './TechniciansList';
import SalespersonHistory from './SalespersonHistory'
import ServiceHistory from './ServiceHistory';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="automobiles/new" element={<AutomobilesForm />} />
          <Route path="automobiles" element={<AutomobilesList automobiles={props.automobiles} />} />
          <Route path="manufacturers/new" element={<ManufacturerForm />} />
          <Route path="manufacturers" element={<ManufacturerList manufacturers={props.manufacturers} />} />
          <Route path="models/new" element={<ModelsForm />} />
          <Route path="models" element={<ModelsList models={props.models} />} />
          <Route path="salespeople/new" element={<SalespersonForm />} />
          <Route path="salespeople" element={<SalespeopleList salesperson={props.salesperson} />} />
          <Route path="salespeople/history" element={<SalespersonHistory salesperson={props.salesperson} />} />
          <Route path="customers/new" element={<CustomerForm />} />
          <Route path="customers" element={<CustomerList customer={props.customer} />} />
          <Route path="sales/new" element={<SaleForm />} />
          <Route path="sales" element={<SaleList sale={props.sale} />} />
          <Route path="technicians/new" element={<TechnicianForm />} />
          <Route path="technicians" element={<TechnicianList technician={props.technician} />} />
          <Route path="appointments/new" element={<AppointmentsForm />} />
          <Route path="appointments" element={<AppointmentsList appointments={props.appointments} />} />
          <Route path="appointments/history" element={<ServiceHistory appointments={props.appointments} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
