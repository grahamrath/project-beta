import { useEffect, useState } from 'react';

function AppointmentsForm() {
    const [vin, setVin] = useState("");
    const [customer, setCustomer] = useState("");
    const [dateTime, setDateTime] = useState("");
    const [technician, setTechnician] = useState("");
    const [technicians, setTechnicians] = useState([]);
    const [reason, setReason] = useState("");

    const handleVinChange = (e) => {
        const value = e.target.value;
        setVin(value);
    };
    const handleCustomerChange = (e) => {
        const value = e.target.value;
        setCustomer(value);
    };
    const handleDateTimeChange = (e) => {
        const value = e.target.value;
        setDateTime(value);
    };
    const handleTechnicianChange = (e) => {
        const value = e.target.value;
        setTechnician(value);
    };
    const handleReasonChange = (e) => {
        const value = e.target.value;
        setReason(value);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date_time = dateTime;
        data.technician = technician;
        data.reason = reason;

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();

            setVin('');
            setCustomer('');
            setDateTime('');
            setTechnician('');
            setReason('');
        };
    };

    const fetchdata = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technician);
        };
    };

    useEffect(() => {
        fetchdata();
    }, []);

    return (
        <>
        <div className='row'>
        <div className='offset-3 col-6'>
            <div className='shadow p-4 mt-4'>
            <h1>Create an Appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
                <input value={vin}
                onChange={handleVinChange}
                placeholder="VIN"
                required type="text"
                name="vin"
                id="vin"
                className="form-control"/>
                <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
                <input value={customer}
                onChange={handleCustomerChange}
                placeholder="Customer"
                required type="text"
                name="customer"
                id="customer"
                className="form-control"/>
                <label htmlFor="customer">Customer</label>
            </div>
            <div className="form-floating mb-3">
                <input value={dateTime}
                onChange={handleDateTimeChange}
                placeholder="Date and Time"
                required type="datetime-local"
                name="date_time"
                id="date_time"
                className="form-control"/>
                <label htmlFor="date-time">Enter date and Time</label>
            </div>
            <div className='mb-3'>
            <select value={technician}
            onChange={handleTechnicianChange}
            required name="technician"
            id="technician"
            className="form-select">
                    <option>Choose a Technician</option>
                    {technicians.map(technician => {
                        return (
                            <option key={technician.id} value={technician.id}>
                                {technician.first_name} {technician.last_name}
                            </option>
                        );
                    })};
            </select>
            </div>
            <div className="form-floating mb-3">
                <input value={reason}
                onChange={handleReasonChange}
                placeholder="Reason"
                required type="text"
                name="reason"
                id="reason"
                className="form-control"/>
                <label htmlFor="reason">Reason</label>
            </div>
            <button className='btn btn-primary'>Create</button>
            </form>
            </div>
        </div>
        </div>
        </>
    )

}



export default AppointmentsForm;
