import { useEffect, useState } from 'react';

function AppointmentsList() {
    const [appointments, setAppointments] = useState([])
    const [vins, setVins] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setAppointments(data.appointments);
        }

        const responseAutos = await fetch('http://localhost:8100/api/automobiles/');
        if (responseAutos.ok) {
            const data = await responseAutos.json();
            console.log(data)
            const vins = data.autos.map(auto => auto.vin);
            setVins(vins)
        }
    }



    useEffect(()=>{
        getData()
    }, [])

    const finishAppointment = async(id) => {
        const fetchConfig = {
            method: "PUT",
            "Content-Type": "application/json",
        }
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, fetchConfig)
        if (response.ok) {
            getData()
        }
    }

    const cancelAppointment = async(id) => {
        const fetchConfig = {
            method: "PUT",
            "Content-Type": "application/json",
        }
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, fetchConfig)
        if (response.ok) {
            getData()
        }
    }

    return (
        <div>
        <h1>Service appointments</h1>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>VIN</th>
                <th>is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
            </tr>
            </thead>
            <tbody>
            {appointments.filter(appointment => appointment.status === "pending").map (appointment => {
                return (
                <tr key={appointment.id}>
                    <td>{ appointment.vin }</td>
                    <td>{ vins.includes(appointment.vin) ? 'yes':'no' }</td>
                    <td>{ appointment.customer }</td>
                    <td>{ new Date(appointment.date_time).toDateString() }</td>
                    <td>{ new Date(appointment.date_time).toLocaleTimeString() }</td>
                    <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                    <td>{ appointment.reason }</td>
                    <td>
                        <button type="button" id={appointment.id} onClick={() => finishAppointment(appointment.id)} className="btn btn-success">
                            Finish
                        </button>
                        <button type="button" id={appointment.id} onClick={() => cancelAppointment(appointment.id)} className="btn btn-danger">
                            Cancel
                        </button>
                    </td>

                </tr>
                );
            })}
            </tbody>
        </table>
        </div>
    )

}
export default AppointmentsList;
