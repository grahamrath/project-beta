import React, { useEffect, useState } from 'react';

function CustomerForm(props) {
    const [first_name, setFirstName] = useState("");
    const [last_name, setLastName] = useState("");
    const [address, setAddress] = useState("");
    const [phone_number, setPhoneNumber] = useState("");

    const handleFirstNameChange = (e) => {
        const value = e.target.value;
        setFirstName(value);
    };

    const handleLastNameChange = (e) => {
        const value = e.target.value;
        setLastName(value);
    };

    const handleAddressChange = (e) => {
        const value = e.target.value;
        setAddress(value);
    };

    const handlePhoneNumberChange = (e) => {
        const value = e.target.value;
        setPhoneNumber(value);
    };


    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.address = address;
        data.phone_number = phone_number;

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();

            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');
        };
    };

    return (
        <>
            <div className='row'>
                <div className='offset-3 col-6'>
                    <div className='shadow p-4 mt-4'>
                        <h1>Add a Customer</h1>
                        <form onSubmit={handleSubmit} id="create-customer-form">
                            <div className="form-floating mb-3">
                                <input value={first_name}
                                    onChange={handleFirstNameChange}
                                    placeholder="First Name"
                                    required type="text"
                                    name="first_name"
                                    id="first_name"
                                    className="form-control" />
                                <label htmlFor="name">First Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={last_name}
                                    onChange={handleLastNameChange}
                                    placeholder="Last Name"
                                    required type="text"
                                    name="last_name"
                                    id="last_name"
                                    className="form-control" />
                                <label htmlFor="name">Last Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={address}
                                    onChange={handleAddressChange}
                                    placeholder="Address"
                                    required type="text"
                                    name="address"
                                    id="address"
                                    className="form-control" />
                                <label htmlFor="name">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={phone_number}
                                    onChange={handlePhoneNumberChange}
                                    placeholder="Phone Number"
                                    required type="text"
                                    name="phone_number"
                                    id="phone_number"
                                    className="form-control" />
                                <label htmlFor="name">Phone Number</label>
                            </div>
                            <button className='btn btn-primary'>Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}
export default CustomerForm;
