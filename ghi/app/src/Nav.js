import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Manufacturers
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/manufacturers"> List Manufacturers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/manufacturers/new">Create a Manufacturer</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Models
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/models"> List Models</NavLink></li>
                <li><NavLink className="dropdown-item" to="/models/new">Create a Model</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Automobiles
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/automobiles"> List Automobiles</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles/new">Create an Automobile</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/sales/new">Record a New Sale</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales"> List Sales</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Salespeople
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/salespeople/new">Add a Salesperson</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople"> List Salespeople</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople/history">Salesperson History</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Appointments
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/appointments"> List Appointments</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/new">Schedule an Appointment</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/history">Appointment History</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Technicians
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/technicians"> List Technicians</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians/new">Add a Technician</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Customers
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/customers"> List Customers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customers/new">Add a Customer</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
