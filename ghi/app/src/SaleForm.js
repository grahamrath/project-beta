import React, { useEffect, useState } from 'react';

function SaleForm() {
  const [vin, setVin] = useState('');
  const [automobiles, setAutomobiles] = useState([]);
  const [salesperson, setSalesperson] = useState('');
  const [salespeople, setSalespeople] = useState([]);
  const [customer, setCustomer] = useState('');
  const [customers, setCustomers] = useState([]);
  const [price, setPrice] = useState('');


  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  }

  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSalesperson(value);
  }

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }

  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.automobile = vin
    data.salesperson = salesperson;
    data.customer = customer;
    data.price = price;

    const updateURL = `http://localhost:8100/api/automobiles/${vin}/`
    const updateConfig = {
      method: "put",
      body: JSON.stringify({sold: true}),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const updateresponse = await fetch(updateURL, updateConfig);
    if (updateresponse.ok) {
      const updateSale = await updateresponse.json();
    }

    const salesUrl = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(salesUrl, fetchConfig);
    if (response.ok) {
      const newSale = await response.json();
      setVin([]);
      setSalesperson([]);
      setCustomer([]);
      setPrice('');
    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  }
  useEffect(() => {
    fetchData();
  }, []);

  const fetchDataOne = async () => {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customer);
    }
  }
  useEffect(() => {
    fetchDataOne();
  }, []);

  const fetchDataTwo = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salesperson);
    }
  }
  useEffect(() => {
    fetchDataTwo();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a New sale</h1>
          <form onSubmit={handleSubmit} id="create-sale-form">
          <div className="mb-3">
            <h6>Automobile Vin</h6>
              <select onChange={handleVinChange}
              required name="vin"
              id="vin"
              className="form-select"
              value={vin}>
                <option value="">Automobile VIN</option>
                {automobiles.filter(automobile => automobile.sold == false).map(automobile => {
                  return (
                    <option key={automobile.vin} value={automobile.vin}>
                      {`${automobile.vin}`}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
            <h6>Salesperson</h6>
              <select onChange={handleSalespersonChange}
              required name="salesperson"
              id="salesperson"
              className="form-select"
              value={salesperson}>
                <option value="">Choose a Salesperson</option>
                {salespeople.map(salesperson => {
                  return (
                    <option key={salesperson.id} value={salesperson.id}>
                      {salesperson.first_name} {salesperson.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
            <h6>Customer</h6>
              <select onChange={handleCustomerChange}
              required name="customer"
              id="customer"
              className="form-select"
              value={customer}>
                <option value="">Choose a Customer</option>
                {customers.map(customer => {
                  return (
                    <option key={customer.id} value={customer.id}>
                      {customer.first_name} {customer.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
            <h6>Price</h6>
              <input onChange={handlePriceChange}
              placeholder="0"
              required type="number"
              id="price"
              className="form-label"
              value={price} />
              <label htmlFor="price"></label>
            </div>
            <div>
              <button className="btn btn-primary">Create</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SaleForm;
