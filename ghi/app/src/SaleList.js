import { useEffect, useState } from 'react';

function SaleList() {
  const [sale, setSale] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/sales');

    if (response.ok) {
      const data = await response.json();
      setSale(data.sale)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  return (
    <div className="container">
      <h1>Sales</h1>
      <table className="table align-middle table-striped table-hover table-borderless">
        <thead>
          <tr>
            <th scope="col">Salesperson Employee ID</th>
            <th scope="col">Salesperson Name</th>
            <th scope="col">Customer</th>
            <th scope="col">VIN</th>
            <th scope="col">Price</th>
          </tr>
        </thead>
        <tbody>
          {sale.map(sale => {
            return (
              <tr key={sale.id}>
                <td>{sale.salesperson.employee_id}</td>
                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                <td>{sale.automobile.vin}</td>
                <td>${sale.price}.00</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )
}

export default SaleList;
