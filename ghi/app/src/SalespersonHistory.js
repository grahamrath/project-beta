import { useEffect, useState } from 'react';

function SalespersonHistoryList() {
    const [sale, setSale] = useState([]);
    const [filterSales, setFilterSales] = useState ([])
    const [salesperson, setSalesperson] = useState('');
    const [salespeople, setSalespeople] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');

        if (response.ok) {
            const data = await response.json();
            setSale(data.sale)
        }
    }
    useEffect(() => {
        getData()
    }, [])

    const getPerson = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salesperson)
        }
    }
    useEffect(() => {
        getPerson()
    }, [])

    const filterAllSales = () => {
        if (salesperson === '') {
            setFilterSales(sale)
        } else {
            const filterSalesList = sale.filter(sale => sale.salesperson.id.toString() === salesperson);
            setFilterSales(filterSalesList)
        }
    }
    useEffect(() => {
        filterAllSales();
    }, [salesperson, sale])

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
      }

    return (
        <div className="container">
            <h1>Salesperson Sale History</h1>
            <select onChange={handleSalespersonChange}
              required name="salesperson"
              id="salesperson"
              className="form-select"
              value={salesperson}>
                <option value="">Choose a Salesperson</option>
                {salespeople.map(salesperson => {
                  return (
                    <option key={salesperson.id} value={salesperson.id}>
                      {salesperson.first_name} {salesperson.last_name}
                    </option>
                  );
                })}
              </select>
            <table className="table align-middle table-striped table-hover table-borderless">
                <thead>
                    <tr>
                        <th scope="col">Salesperson</th>
                        <th scope="col">Customer</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Price</th>
                    </tr>
                </thead>
                <tbody>
                {filterSales.map(sale => {
                    return (
                    <tr key={sale.id}>
                        <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                        <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                        <td>{sale.automobile.vin}</td>
                        <td>${sale.price}.00</td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
    )
}

export default SalespersonHistoryList;
