import { useEffect, useState } from 'react';

function ServiceHistory() {
    const [appointments, setAppointments] = useState([])
    const [vins, setVins] = useState('')
    const [filterAppointments, setFilterAppointments] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setAppointments(data.appointments);
        }


        const responseAutos = await fetch('http://localhost:8100/api/automobiles/');
        if (responseAutos.ok) {
            const data = await responseAutos.json();
            console.log(data)
            const vins = data.autos.map(auto => auto.vin);
            setVins(vins)
        }

    }

    const handleFilter = (event) => {
        const searchVin = event.target.value
        setFilterAppointments(searchVin)
    };

    let filter = appointments;
    if (filterAppointments.length > 0) {
        filter = appointments.filter(appointment =>
            appointment.vin.toUpperCase().includes(filterAppointments.toUpperCase()))
    }

    useEffect(()=>{
        getData()
    }, [])


    return (
        <div className="container">
            <h1>Service History</h1>
            <div className="input-group mb-3">
                <input type="text"
                className="form-control"
                aria-label="VIN"
                aria-describedby='button-addon2'
                onChange={handleFilter}
                placeholder="type in VIN to search"
                value={filterAppointments}
                data={appointments} />
                <button className="btn btn-outline-secondary"
                type="button"
                id="button-addon2">Search by vin</button>
            </div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>VIN</th>
                <th>is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            {filter.map (appointment => {
                return (
                <tr key={appointment.id}>
                    <td>{ appointment.vin }</td>
                    <td>{ vins.includes(appointment.vin) ? 'yes':'no' }</td>
                    <td>{ appointment.customer }</td>
                    <td>{ new Date(appointment.date_time).toDateString() }</td>
                    <td>{ new Date(appointment.date_time).toLocaleTimeString() }</td>
                    <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                    <td>{ appointment.reason }</td>
                    <td>{ appointment.status }</td>
                </tr>
                );
            })}
            </tbody>
        </table>
        </div>
    )
}
export default ServiceHistory;
