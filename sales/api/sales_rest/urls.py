from django.urls import path
from .views import (api_list_salesperson, api_salesperson_details,
                    api_list_customers, api_customer_details, api_list_sales,
                    api_sale_details)

urlpatterns = [
    path("salespeople/", api_list_salesperson, name="api_list_salesperson"),
    path(
        "salespeople/<int:pk>/",
        api_salesperson_details,
        name="api_salesperson_details"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path(
        "customers/<int:pk>/",
        api_customer_details,
        name="api_customer_details"),
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:pk>/", api_sale_details, name="api_sale_details"),
]
