from django.db import models

# Create your models here.


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=50)


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    status_options = [
        ('pending', 'Pending'),
        ('finished', 'Finished'),
        ('canceled', 'Canceled')
    ]
    date_time = models.DateTimeField(auto_now_add=False)
    reason = models.CharField(max_length=500)
    status = models.CharField(max_length=50,
                              choices=status_options,
                              default='pending')
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.vin

    class Meta:
        ordering = ("date_time",)
